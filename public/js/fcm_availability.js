var fcmCheckBtn = document.getElementById("fcmCheck");
var fcmSendBtn = document.getElementById("fcmSend");
var fcmResult = document.getElementById('fcmResult');

fcmCheckBtn.onclick = function () {
  // Ensure that the user can receive Safari Push Notifications.
  if ('ServiceWorker' in window && 'serviceWorker' in navigator) {
    logResponseEvent('Firebase Cloud Messaging supported');
    fcmResult.innerText = 'Supported';
  } else {
    logResponseEvent('Firebase Cloud Messaging not Supported');
    fcmResult.innerText = 'No Support';
  }
};

fcmSendBtn.onclick = function () {
  // The web service URL is a valid push provider, and the user said yes.
  // permissionData.deviceToken is now available to use.
  var title = 'Hello';
  var body = 'Welcome to Laravel notification through FCM';
  var tag = 'test';
  sendNotification(title, body, tag);
};
