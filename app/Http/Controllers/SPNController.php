<?php

namespace App\Http\Controllers;

use App\Services\SPNFactoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SPNController extends Controller
{
    /**
     * @param $websitePushID
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadPushPackage($websitePushID, Request $request)
    {
        $validation = (new SPNFactoryService())->validatePushDownload($websitePushID);

        if ($validation) {

            $notificationPacket = '../storage/spn/NotificationPacket.pushPackage.zip';
            $headers = [
                'Content-type: application/zip'
            ];
            Log::notice("Request: {$websitePushID}");
            Log::debug($request);
            return response()->file($notificationPacket, $headers);
        }
    }

    /**
     * @param $deviceToken
     * @param $websitePushID
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function devicePermission($deviceToken, $websitePushID, Request $request)
    {
        Log::notice("(Register) Device: {$deviceToken}, PushID: {$websitePushID}");
        Log::debug($request);
        return response()->json(['status' => 'true'], 200);
    }

    /**
     * @param $deviceToken
     * @param $websitePushID
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgetDevice($deviceToken, $websitePushID, Request $request)
    {
        Log::notice("(Forget) Device: {$deviceToken}, PushID: {$websitePushID}");
        Log::debug($request);
        return response()->json(['status' => 'true'], 200);
    }

    /**
     * @param Request $request
     */
    public function logError(Request $request)
    {
        Log::debug($request);
        Log::error($request);
    }
}
