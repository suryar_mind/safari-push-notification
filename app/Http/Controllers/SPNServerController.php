<?php

namespace App\Http\Controllers;

use App\Services\ApplePushService;
use Illuminate\Http\Request;

class SPNServerController extends Controller
{
    /**
     * To send a Notification Payload to Apple Push Notification server
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(Request $request)
    {
        $deviceToken = $request->deviceToken;

        $messageTitle = 'Flight A998 Now Boarding';
        $messageBody = 'A push notification has been sent!';
        $messageAction = 'View';
        $messageUrlArgs = ["boarding", "A998"];

        $payload = [
            'aps' => [
                'alert' => [
                    "title" => $messageTitle,
                    "action-loc-key"=> $messageAction,
                    'body' => $messageBody,
                ],
                "url-args" => $messageUrlArgs
            ]
        ];

        $payloadResponse = (new ApplePushService())->sendApplePushNotification($deviceToken, $payload);

        return response()->json(['result' => $payloadResponse, 'message' => $payload], 200);
    }
}
