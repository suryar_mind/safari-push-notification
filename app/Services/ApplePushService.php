<?php

namespace App\Services;

class ApplePushService
{
    private $privateCertificatePath = '../storage/spn/certificate.pem';
    private $passphrase = 'jjcanincuienwcunwiuefhweuifwenfiecw';
    private $remoteSocket = 'ssl://gateway.push.apple.com:2195';

    /**
     * To send a Notification Payload to Apple Push Notification
     *
     * @param $deviceToken
     * @param $payload
     * @return bool
     */
    public function sendApplePushNotification($deviceToken, $payload)
    {
        $payload = json_encode($payload);

        $streamContext = stream_context_create();
        stream_context_set_option($streamContext, 'ssl', 'local_cert', $this->privateCertificatePath);
        stream_context_set_option($streamContext, 'ssl', 'passphrase', $this->passphrase);

        $err = null;
        $errStr = null;
        $commTunnel = stream_socket_client($this->remoteSocket, $err, $errStr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $streamContext);
        if (!$commTunnel) {
            return false;
        }
        $messagePacket = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
        if (!$messagePacket) {
            return false;
        }
        $result = fwrite($commTunnel, $messagePacket, strlen($messagePacket));
        fclose($commTunnel);

        return true;
    }
}
