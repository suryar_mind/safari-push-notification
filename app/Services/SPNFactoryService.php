<?php


namespace App\Services;


class SPNFactoryService
{
    private $websitePushId = 'web.com.herokuapp.rocky-harbor-52848';

    public function validatePushDownload($spnPushId)
    {
        return $spnPushId === $this->websitePushId;
    }

    public function devicePermission($deviceToken, $websitePushID)
    {
        //TODO - Device registration permission
        return true;
    }

    public function forgetDevice($deviceToken, $websitePushID)
    {
        // TODO - Device de-registration permission
        return true;
    }

    public function logError()
    {
        // TODO - Log any error occurred
        return true;
    }
}