<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 50px;
            }

            .split {
                height: 100%;
                position: fixed;
                z-index: 1;
                top: 0;
                overflow-x: hidden;
                padding-top: 20px;
            }

            .left {
                left: 0;
                width: 70%;
            }

            .right {
                right: 0;
                width: 30%;
                border-left: 2px solid #000000;
            }

            .centered {
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                text-align: center;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>

        <link rel="manifest" href="/manifest.json" />
        <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
        <script>
            var OneSignal = window.OneSignal || [];
            OneSignal.push(function() {
                OneSignal.init({
                    appId: "f018f85e-2780-4970-8d6e-44fcf4855db2",
                });
            });
        </script>
    </head>
    <body>
        <div>
            <div class="split left">

                <div class="content centered">
                    <div class="title m-b-md">
                        Safari Push Notification
                    </div>

                    <div class="links">
                        <a href="javascript:void(0)" id="spnCheck">Check SPN Suport</a>
                        <a href="javascript:void(0)" id="spnSend">Send</a>

                        <br>

                        <form action="/spn-send" method="post" class="form-inline form">
                            @csrf
                            <label>
                                Apple Device token:
                                <input name="deviceToken" type="text" class="form-control">
                            </label>
                            <input type="submit" class="btn btn-sm btn-success">
                        </form>

                        <br><br>

                        <a href="javascript:void(0)" id="fcmCheck">Check FCM Support</a>
                        <a href="javascript:void(0)" id="fcmSend">Send</a>
                    </div>

                    <hr>

                    <div class="links">
                        SPN: <span id="spnResult"></span>
                        <br>
                        FCM: <span id="fcmResult"></span>
                    </div>

                </div>
                <div class='onesignal-customlink-container'></div>
            </div>
            <div class="split right">
                <div>
                    <strong>Logs</strong>
                    <hr>
                    <ul id="logsDiv"></ul>
                </div>
            </div>

        </div>

        <script src="{{ mix('/js/logs.js') }}"></script>
        <script src="{{ mix('/js/spn_availability.js') }}"></script>
        <script src="{{ mix('/js/fcm_availability.js') }}"></script>
    </body>
</html>
