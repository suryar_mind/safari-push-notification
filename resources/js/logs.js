
var logsDiv = document.getElementById('logsDiv');

var sendNotification = function (title, body, tag) {
    var n = new Notification(title, { body, tag } );
    logResponseEvent('Notification payload sent');
};

function logResponseEvent (message) {
    var node = document.createElement("li");
    var textnode = document.createTextNode(message);
    node.appendChild(textnode);
    logsDiv.prepend(node);
}