var spnCheckBtn = document.getElementById("spnCheck");
var spnSendBtn = document.getElementById("spnSend");
var spnResult = document.getElementById('spnResult');

var websitePushId = 'web.com.herokuapp.rocky-harbor-52848';
var webServiceUrl = 'https://rocky-harbor-52848.herokuapp.com/api';

var spnPermission;

spnCheckBtn.onclick = function() {
    // Ensure that the user can receive Safari Push Notifications.
    if ('safari' in window && 'pushNotification' in window.safari) {
        var permissionData = window.safari.pushNotification.permission(websitePushId);
        checkRemotePermission(permissionData);
    } else {
        logResponseEvent('Safari Push Notification not Supported');
        spnResult.innerText = 'No Support for SPN';
    }
};

var checkRemotePermission = function (permissionData) {
    spnPermission = permissionData;
    console.log(spnPermission);
    if (permissionData.permission === 'default') {
        // This is a new web service URL and its validity is unknown.
        window.safari.pushNotification.requestPermission(
            webServiceUrl, // The web service URL.
            websitePushId,     // The Website Push ID.
            {}, // Data that you choose to send to your server to help you identify the user.
            checkRemotePermission         // The callback function.
        );

        logResponseEvent('Safari Push Notification permission is not handled');
        spnResult.innerText = 'Default';

    } else if (permissionData.permission === 'granted') {
        logResponseEvent('Safari Push Notification granted for this site');
        spnResult.innerText = 'Granted';
    } else if (permissionData.permission === 'denied') {
        // The user said no.
        checkRemotePermission();
        logResponseEvent('Safari Push Notification denied for this site');
        spnResult.innerText = 'Denied';
    }
};

spnSendBtn.onclick = function () {
    if(spnPermission.permission === 'granted') {
        // The web service URL is a valid push provider, and the user said yes.
        // permissionData.deviceToken is now available to use.
        var title = 'Hello';
        var body = 'Welcome to Laravel notification through SPN';
        var tag = 'test';

        sendNotification(title, body, tag);
        logResponseEvent('Sent SPN notification');
    } else if(spnPermission.permission === 'default') {
        logResponseEvent('SPN notification not ready');
    } else if(spnPermission.permission === 'denied') {
        logResponseEvent('SPN notification blocked');
    }

};