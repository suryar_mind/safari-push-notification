const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.babel('resources/js/logs.js' ,'public/js/logs.js').version();
mix.babel('resources/js/spn_availability.js' ,'public/js/spn_availability.js').version();
mix.babel('resources/js/fcm_availability.js' ,'public/js/fcm_availability.js').version();
