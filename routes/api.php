<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/v2')->group(function () {
    Route::post('/pushPackages/{websitePushID}', 'SPNController@downloadPushPackage');
    Route::post('/devices/{deviceToken}/registrations/{websitePushID}', 'SPNController@devicePermission');
    Route::delete('/devices/{deviceToken}/registrations/{websitePushID}', 'SPNController@forgetDevice');
    Route::post('/log', 'SPNController@logError');
});
